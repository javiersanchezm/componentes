const principal = document.querySelector(".principal");
const secundarios = document.querySelectorAll(".secundario");

//Seleccionar todos las imagenes pequeñas y recorrer con foreach
secundarios.forEach( secundario => {
    //A cada uno de los elementos se le a agregar un evento
    secundario.addEventListener( ("click"), function() {
        const active = document.querySelector(".active");
        active.classList.remove("active");
        //this hace referencia al elemento secundario
        this.classList.add("active");
        principal.src = this.src;
    });
})



//Carrousel
//https://nickpiscitelli.github.io/Glider.js/
//https://www.jsdelivr.com/package/npm/glider-js

window.addEventListener('load', ()=> {
    new Glider(document.querySelector('.elementos'), {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: '.indicador',
        arrows: {
            prev: '.anterior',
            next: '.siguiente'
        },
        responsive: [
            {
                breakpoint: 775,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
            },
            {
                breakpoint: 1024,
                settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                itemWidth: 150,
                duration: 0.25
            }
            }
        ]
    });
});




// Galería de paisajes
var imagenes = [1,2,3,4,5,6,7,8,9,10,11];
var galeria = document.getElementById('galery');

for( imagen of imagenes){
    galeria.innerHTML += `
        <a href="#" data-bs-toggle="modal" data-bs-target="#id${imagen}">
            <img src="img/image${imagen}.jpg" alt="" class="card-img-top p-2">
        </a>
        <div class="modal fade" id="id${imagen}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <img src="img/image${imagen}.jpg" alt="" class="img-fluid rounded">  
            </div>
        </div>
    `;
}
